package com.example.PHY2APP.fragments.adapters


import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.PHY2APP.R
import android.bluetooth.BluetoothDevice

import android.util.Log
import android.widget.*

import java.lang.Exception


class BlueToothExpandableListAdapter internal constructor(
    private val context: Context,
    private val titleList: List<String>,
    private val dataList: LinkedHashMap<String, List<String>>,
    private val devicelist: List<BluetoothDevice>

) : BaseExpandableListAdapter() {

    override fun getChild(listPosition: Int, expandedListPosition: Int): Any {
        return this.dataList[this.titleList[listPosition]]!![expandedListPosition]
    }

    override fun getChildId(listPosition: Int, expandedListPosition: Int): Long {
        return expandedListPosition.toLong()
    }

    override fun getChildView(
        listPosition: Int,
        expandedListPosition: Int,
        isLastChild: Boolean,
        convertView: View?,
        parent: ViewGroup
    ): View {
        var convertView = convertView
        val expandedListText = getChild(listPosition, expandedListPosition) as String
        if (convertView == null) {
            val layoutInflater =
                this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.list_item, null)
        }
        val expandedListTextView = convertView!!.findViewById<TextView>(R.id.expandedListItem)
        expandedListTextView.text = expandedListText
        return convertView
    }

    override fun getChildrenCount(listPosition: Int): Int {
        return this.dataList[this.titleList[listPosition]]!!.size
    }

    override fun getGroup(listPosition: Int): Any {
        return this.titleList[listPosition]
    }

    override fun getGroupCount(): Int {
        return this.titleList.size
    }

    override fun getGroupId(listPosition: Int): Long {
        return listPosition.toLong()
    }

    override fun getGroupView(
        listPosition: Int,
        isExpanded: Boolean,
        convertView: View?,
        parent: ViewGroup
    ): View {
        var convertView = convertView
        val listTitle = getGroup(listPosition) as String
        val device = devicelist[listPosition]
        val child = getChild(listPosition, 1) as String


        if (convertView == null) {
            val layoutInflater =
                this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.list_group, null)
        }
        val listTitleTextView = convertView!!.findViewById<TextView>(R.id.listTitle)
        listTitleTextView.setTypeface(null, Typeface.BOLD)
        listTitleTextView.text = listTitle
        var switchBluetooth = convertView!!.findViewById<Switch>(R.id.switchWifi)


        switchBluetooth.setOnCheckedChangeListener { _, isChecked ->
            //var c =item[0].getKey().toString()
            if (isChecked) {
                Log.d("connect.... ", "$child")

                Toast.makeText(
                    context,
                    " connecting..$listTitle",
                    Toast.LENGTH_SHORT
                ).show()
                connectToBluetooth(device)
                //("AndroidWi")
            } else {
                disableBluetooth(device)
                Log.d("disconnect.... ", "$child")
                Toast.makeText(
                    context,
                    " disconnecting..$child",
                    Toast.LENGTH_SHORT
                ).show()

            }
        }



        return convertView
    }

    private fun connectToBluetooth(device: BluetoothDevice) {
        device.createBond();
    }

    private fun disableBluetooth(device: BluetoothDevice) {
        try {
            device.javaClass.getMethod("removeBondNative", Unit as Class<*>)
                .invoke(device, null as Any)


        } catch (e: Exception) {

        }
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun isChildSelectable(listPosition: Int, expandedListPosition: Int): Boolean {
        return true
    }
}