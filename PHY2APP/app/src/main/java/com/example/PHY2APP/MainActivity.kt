package com.example.PHY2APP

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AppCompatActivity



import com.example.PHY2APP.fragments.BluetoothFragment
import com.example.PHY2APP.fragments.WifiFragment
import com.example.PHY2APP.fragments.adapters.ViewPagerAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var Tag : String ="Main Activity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d(Tag , "Oncreate")
        var mViewPager : ViewPager = view_pager
        setUpViewPager(mViewPager)
        tabs.setupWithViewPager(mViewPager)

    }
    private fun setUpViewPager(viewPager: ViewPager){
        val adapter= ViewPagerAdapter(supportFragmentManager)

    adapter.addFragment(WifiFragment(),"wifi")
    adapter.addFragment(BluetoothFragment(),"Bluetooth")
        viewPager.adapter=adapter
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when(item.itemId ) {

      // R.id.AboutId -> supportFragmentManager.beginTransaction().replace(R.id.fragment_container, AboutFragment()).commit()
           R.id.AboutId ->{
               startActivity(Intent(this,AboutActivity::class.java))
           }
            R.id.exitId ->   finish();




        }
        return true
    }
}