package com.example.PHY2APP.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.wifi.ScanResult
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.example.PHY2APP.R
import kotlinx.android.synthetic.main.fragment_bluetooth.view.*
import androidx.core.app.ActivityCompat
import com.example.PHY2APP.fragments.adapters.BlueToothExpandableListAdapter
import java.util.ArrayList

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
val request_enable_bluetooth = 1

/**
 * A simple [Fragment] subclass.
 * Use the [BluetoothFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class BluetoothFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    var bluetoothManager: BluetoothManager? = null
    private val size = 0
    private var results: List<ScanResult>? = null
    val item = HashMap<String, String>()
    private var arrayListSSID = arrayListOf<String>()
    private var arrayListMac = arrayListOf<String>()
    internal var expandableListView: ExpandableListView? = null
    internal var adapter: ExpandableListAdapter? = null
    internal var titleList: List<String>? = null
    val listData = java.util.LinkedHashMap<String, List<String>>()
    val devices = ArrayList<BluetoothDevice>()
    private var listView: ListView? = null
    private val mDeviceList = ArrayList<String>()
    lateinit var mBluetoothAdapter: BluetoothAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    @SuppressLint("UseRequireInsteadOfGet")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var mcontext = getContext();
        var view: View = inflater.inflate(R.layout.fragment_bluetooth, container, false)
        bluetoothManager = getActivity()?.getApplicationContext()
            ?.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
            ActivityCompat.checkSelfPermission(
                getContext()!!,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), 0)

        } else {
            //  listView = view.findViewById(R.id.listView) as ListView
            expandableListView = view.findViewById(R.id.expandableListView)

            val pm = context!!.packageManager
            val hasBluetooth = pm.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH)
            scanbluetooth()
        }



        view.refreshBId.setOnClickListener { view ->
            Toast.makeText(
                context,
                 "Refreshing...",
                Toast.LENGTH_SHORT
            ).show()
            scanbluetooth()

        }



        return view
    }

    fun scanbluetooth() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        mBluetoothAdapter.startDiscovery()
        val filter = IntentFilter(BluetoothDevice.ACTION_FOUND)
        getActivity()?.registerReceiver(mReceiver, filter)

    }

    override fun onDestroy() {
        getActivity()?.unregisterReceiver(mReceiver)

        super.onDestroy()
    }

    private val mReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device =
                    intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                val Details = ArrayList<String>()

                devices.add(device!!)
                var name = device!!.name
                if (name == null || name == "")
                    name = "-"

                Details.add("name: $name")
                Details.add("address:  ${device.address}")

                listData[name +"/"+device.address] = Details

                titleList = ArrayList(listData.keys)
                // titleList.sortedBy {  }

                adapter =
                    BlueToothExpandableListAdapter(
                        context,
                        titleList as ArrayList<String>,
                        listData,
                        devices as ArrayList<BluetoothDevice>
                    )
                expandableListView!!.setAdapter(adapter)

                expandableListView!!.setOnGroupExpandListener { groupPosition ->
                    Toast.makeText(
                        context,
                        (titleList as ArrayList<String>)[groupPosition] + " List Expanded.",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                expandableListView!!.setOnGroupCollapseListener { groupPosition ->
                    Toast.makeText(
                        context,
                        (titleList as ArrayList<String>)[groupPosition] + " List Collapsed.",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                expandableListView!!.setOnChildClickListener { parent, v, groupPosition, childPosition, id ->
                    Toast.makeText(
                        context,
                        "Clicked: " + (titleList as ArrayList<String>)[groupPosition] + " -> " + listData[(titleList as ArrayList<String>)[groupPosition]]!!.get(
                            childPosition
                        ),
                        Toast.LENGTH_SHORT
                    ).show()
                    false

                }

                Log.i(
                    "BT1", """
     ${device.name}
     ${device.address}
     """.trimIndent()
                )

            }
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        val a: Activity
        if (context is Activity) {
            a = context
        }
    }


    companion object {
        val extra_address: String = "Device_address"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment BluetoothFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            BluetoothFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}