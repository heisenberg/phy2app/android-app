package com.example.PHY2APP.fragments.adapters

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.PHY2APP.R

import android.net.wifi.WifiConfiguration
import android.net.wifi.WifiManager
import android.util.Log
import android.widget.*
import android.net.wifi.p2p.WifiP2pConfig


class CustomExpandableListAdapter internal constructor(
    private val context: Context,
    private val titleList: List<String>,
    private val dataList: LinkedHashMap<String, List<String>>
) : BaseExpandableListAdapter() {
    var wifiManager: WifiManager? = null
    override fun getChild(listPosition: Int, expandedListPosition: Int): Any {
        return this.dataList[this.titleList[listPosition]]!![expandedListPosition]
    }

    override fun getChildId(listPosition: Int, expandedListPosition: Int): Long {
        return expandedListPosition.toLong()
    }

    override fun getChildView(
        listPosition: Int,
        expandedListPosition: Int,
        isLastChild: Boolean,
        convertView: View?,
        parent: ViewGroup
    ): View {
        var convertView = convertView
        val expandedListText = getChild(listPosition, expandedListPosition) as String
        if (convertView == null) {
            val layoutInflater =
                this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.list_item, null)
        }
        val expandedListTextView = convertView!!.findViewById<TextView>(R.id.expandedListItem)
        expandedListTextView.text = expandedListText
        return convertView
    }

    override fun getChildrenCount(listPosition: Int): Int {
        return this.dataList[this.titleList[listPosition]]!!.size
    }

    override fun getGroup(listPosition: Int): Any {
        return this.titleList[listPosition]
    }

    override fun getGroupCount(): Int {
        return this.titleList.size
    }

    override fun getGroupId(listPosition: Int): Long {
        return listPosition.toLong()
    }

    override fun getGroupView(
        listPosition: Int,
        isExpanded: Boolean,
        convertView: View?,
        parent: ViewGroup
    ): View {
        var convertView = convertView
        val listTitle = getGroup(listPosition) as String
        val childSSID = getChild(listPosition, 0) as String

        if (convertView == null) {
            val layoutInflater =
                this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.list_group, null)
        }
        val listTitleTextView = convertView!!.findViewById<TextView>(R.id.listTitle)
        listTitleTextView.setTypeface(null, Typeface.BOLD)
        listTitleTextView.text = (listTitle.substringBefore('-'))
        var switchWifi = convertView!!.findViewById<Switch>(R.id.switchWifi)


        switchWifi.setOnCheckedChangeListener { _, isChecked ->
            //var c =item[0].getKey().toString()
            if (isChecked) {
                Log.d("connect.... ", "$childSSID")

                Toast.makeText(
                    context,
                     " connecting..$listTitle",
                    Toast.LENGTH_SHORT
                ).show()
                connectToWifi(listTitle.substringBefore('-'),listTitle.substringAfter('-'))
                //("AndroidWi")
            } else {
                disableWiFi()
                Log.d("disconnect.... ", "$childSSID")
                Toast.makeText(
                    context,
                    " disconnecting..$childSSID",
                    Toast.LENGTH_SHORT
                ).show()

            }
        }



        return convertView
    }

    private fun connectToWifi(networkSSID: String,networkBSSID: String) {
        wifiManager = this.context.getApplicationContext()?.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val conf = WifiConfiguration()
        val config = WifiP2pConfig()



        if (!wifiManager!!.isWifiEnabled) {
            wifiManager!!.isWifiEnabled = true
        }

        conf.SSID = "\"" +  networkSSID + "\""

       conf.BSSID = networkBSSID
           //String.format("""%s""", networkBSSID)

        conf.status = WifiConfiguration.Status.ENABLED;
        conf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
        conf.wepTxKeyIndex = 0;
        conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
        conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
        val netId = wifiManager!!.addNetwork(conf)
        wifiManager!!.disconnect()
        wifiManager!!.enableNetwork(netId, true)
        wifiManager!!.reconnect()
    }

    private fun disableWiFi() {
        wifiManager =
            context.getApplicationContext()?.getSystemService(Context.WIFI_SERVICE) as WifiManager
        wifiManager!!.isWifiEnabled = false

    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun isChildSelectable(listPosition: Int, expandedListPosition: Int): Boolean {
        return true
    }
}