package com.example.PHY2APP.fragments

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context

import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.wifi.WifiManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.PHY2APP.R
import android.annotation.SuppressLint
import androidx.core.app.ActivityCompat
import android.app.Activity
import android.net.wifi.ScanResult
import android.net.wifi.WifiConfiguration
import android.os.Build
import android.util.Log
import java.lang.Exception
import kotlinx.android.synthetic.main.fragment_wifi.view.*

import androidx.localbroadcastmanager.content.LocalBroadcastManager
import android.widget.ExpandableListAdapter

import android.widget.ExpandableListView
import com.example.PHY2APP.fragments.adapters.CustomExpandableListAdapter
import android.widget.Toast

import java.util.*
import kotlin.Comparator
import kotlin.collections.HashMap


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [WifiFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class WifiFragment() : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    var wifiManager: WifiManager? = null
    private val size = 0
    private var results: List<ScanResult>? = null
    val item = HashMap<String, String>()
    private var arrayListSSID = arrayListOf<String>()
    private var arrayListMac = arrayListOf<String>()
    internal var expandableListView: ExpandableListView? = null
    internal var adapter: ExpandableListAdapter? = null
    internal var titleList: List<String>? = null
    val listData = java.util.LinkedHashMap<String, List<String>>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

    }

    @SuppressLint("UseRequireInsteadOfGet")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var mcontext = getContext();
        var view: View = inflater.inflate(R.layout.fragment_wifi, container, false)
        wifiManager = getActivity()?.getApplicationContext()
            ?.getSystemService(Context.WIFI_SERVICE) as WifiManager

        expandableListView = view.findViewById(R.id.expandableListView)


        if (!wifiManager!!.isWifiEnabled) {
            Toast.makeText(
                getActivity(),
                "WiFi is disabled ... We need to enable it",
                Toast.LENGTH_LONG
            ).show()
            wifiManager!!.isWifiEnabled = true
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
            ActivityCompat.checkSelfPermission(
                getContext()!!,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), 0)
            //do something if have the permissions
        }  else {
            //do something, permission was previously granted; or legacy device
            scanWifi()


        }

        view.refreshId.setOnClickListener { view ->
            Toast.makeText(
                context,
                "Refreshing...",
                Toast.LENGTH_SHORT
            ).show()
            scanWifi()

        }



        return view
    }

    private fun connectToWifi(networkSSID: String) {
        wifiManager = getActivity()?.getApplicationContext()
            ?.getSystemService(Context.WIFI_SERVICE) as WifiManager

        if (!wifiManager!!.isWifiEnabled) {
            wifiManager!!.isWifiEnabled = true
        }
        val conf = WifiConfiguration()
        conf.SSID = String.format("\"%s\"", networkSSID)
        conf.wepTxKeyIndex = 0;
        conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
        conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
        conf.preSharedKey = java.lang.String.format("\"%s\"", networkSSID)
        val netId = wifiManager!!.addNetwork(conf)
        wifiManager!!.disconnect()
        wifiManager!!.enableNetwork(netId, true)
        wifiManager!!.reconnect()

    }


    private fun disableWiFi() {
        wifiManager = getActivity()?.getApplicationContext()
            ?.getSystemService(Context.WIFI_SERVICE) as WifiManager
        wifiManager!!.isWifiEnabled = false
        Toast.makeText(getActivity(), "Wifi Disabled", Toast.LENGTH_SHORT).show()
    }

    private fun enableWiFi() {
        wifiManager = getActivity()?.getApplicationContext()
            ?.getSystemService(Context.WIFI_SERVICE) as WifiManager
        wifiManager!!.isWifiEnabled = true
        Toast.makeText(getActivity(), "Wifi Enabled", Toast.LENGTH_SHORT).show()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val a: Activity
        if (context is Activity) {
            a = context
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            LocalBroadcastManager.getInstance(requireActivity()).unregisterReceiver(wifiReceiver);
            //getActivity()?.unregisterReceiver(wifiReceiver)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    class Signal_Strength_Comparator : Comparator<ScanResult?> {
        override fun compare(scan0: ScanResult?, scan1: ScanResult?): Int {
            return scan0!!.level.toInt() - scan1!!.level.toInt()
        }
    }
    private fun scanWifi() {
        arrayListSSID.clear()
        arrayListMac.clear()
        try {
            getActivity()?.registerReceiver(
                wifiReceiver,
                IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)
            )
            wifiManager!!.startScan()
            Toast.makeText(getActivity(), "Scanning WiFi ...", Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    var wifiReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            var  resultssss = wifiManager!!.scanResults
            getActivity()?.unregisterReceiver(this)

            results=resultssss.sortedBy { it.level.toInt() }
            results=results!!.reversed()

            for (scanResult in results!!) {

                var wifi_ssid = ""
                wifi_ssid = scanResult.SSID
                Log.d("WIFIScannerActivity", "WIFI SSID: $wifi_ssid")
                var wifi_ssid_first_nine_characters = ""

                if (wifi_ssid.length > 8) {
                    wifi_ssid_first_nine_characters = wifi_ssid.substring(0, 9)
                } else {
                    wifi_ssid_first_nine_characters = wifi_ssid
                }
                Log.d("WIFIScannerActivity", "WIFI SSID 9: $wifi_ssid_first_nine_characters")


                Log.d(
                    "WIFIScannerActivity",
                    "scanResult.SSID: " + scanResult.SSID + ", scanResult.BSSID: " + scanResult.BSSID
                )

                arrayListSSID.add(scanResult.SSID)

                var BSSID = scanResult.BSSID.toString()
                var capabilities = scanResult.capabilities.toString()
                var level = scanResult.level.toString()
                val Details = ArrayList<String>()
                Details.add("BSSID: $BSSID")
                Details.add("capabilities: $capabilities")
                Details.add("level: $level")

                listData[scanResult.SSID+"-"+BSSID] = Details

                //  adapter.notifyDataSetChanged()

                titleList = ArrayList(listData.keys)

                adapter =
                    CustomExpandableListAdapter(context, titleList as ArrayList<String>, listData)
                expandableListView!!.setAdapter(adapter)

                expandableListView!!.setOnGroupExpandListener { groupPosition ->
                    Toast.makeText(
                        context,
                        (titleList as ArrayList<String>)[groupPosition] + " List Expanded.",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                expandableListView!!.setOnGroupCollapseListener { groupPosition ->
                    Toast.makeText(
                        context,
                        (titleList as ArrayList<String>)[groupPosition] + " List Collapsed.",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                expandableListView!!.setOnChildClickListener { parent, v, groupPosition, childPosition, id ->
                    Toast.makeText(
                        context,
                        "Clicked: " + (titleList as ArrayList<String>)[groupPosition] + " -> " + listData[(titleList as ArrayList<String>)[groupPosition]]!!.get(
                            childPosition
                        ),
                        Toast.LENGTH_SHORT
                    ).show()
                    false

                }

                wifi_ssid = ""
                wifi_ssid_first_nine_characters = ""
            }
        }
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment WifiFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            WifiFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}



