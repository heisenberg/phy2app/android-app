package com.example.PHY2APP.fragments.adapters

import android.content.Context
import android.net.wifi.ScanResult
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView
import com.example.PHY2APP.R

class ListAdaptor(context: Context?, wifiList: ListView) : BaseAdapter() {
    lateinit var context: Context
    lateinit var inflater: LayoutInflater
    lateinit var wifiList: List<ScanResult>

    fun ListAdaptor(context: Context, wifiList: List<ScanResult>) {
        this.context = context
        this.wifiList = wifiList
        inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    }

    override fun getCount(): Int {
        return wifiList.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View, parent: ViewGroup?): View? {
        var holder: Holder
        var view: View = convertView

        if (view == null) {
            view = inflater.inflate(R.layout.simple_list_item, null)
            holder = Holder()
            holder.tvDetails = view.findViewById(R.id.txtWifiName)
            view.setTag(holder)
        } else {

            holder = view.getTag() as Holder
        }
        return null
    }
}

class Holder {
    lateinit var tvDetails: TextView

}
